# CTP Standalone
A standalone version of the RSNA endorsed [Clinical Trial Processor (CTP)](https://mircwiki.rsna.org/index.php?title=MIRC_CTP) software to anonymize data
and upload to the [Euro-BioImaging Medical Imaging XNAT Archive](https://xnat.bmia.nl/). Simply download the repository, unpack it, and follow one of the
manuals to get started!

In the ``Manuals'' folder, you can currently find manuals specifically for:
- Uploading data to the MICCAI 2024 AFRICAI repository, for which above XNAT is used.

# Information
Compared to the default CTP software, we have made the following changes to come to this package:
- We have added standalone, executable Java distributions so you do not need to install these on your system, and provide command-line scripts to let 
  CTP use these.
- We have configured the CTP to automatically send data to the Euro-BioImaging Medical Imaging XNAT Archive.
- We have added several configurations based on our experience e.g., as [Euro-BioImaging Population Imaging Flagship Node](https://www.eurobioimaging.eu/nodes/population-imaging-flagship-node-rotterdam), our role in the Dutch Health Research Infrastructure [Health-RI](https://www.health-ri.nl/en/services/xnat), and projects
  such as the [EU H2020 EuCanImage project](https://eucanimage.eu/). These include best practices for anynimization while maintaining important information.
 
# Licenses
The CTP distribution redistributed here is obtained from http://mirc.rsna.org/ according to the [provided instructions by CTP](https://mircwiki.rsna.org/index.php?title=MIRC_CTP).

We provide a Java distribution in the CTPStandAlone/CommonFiles folder, which is lincensed under the [Oracle No-Fee Terms and Conditions (NFTC)](https://www.java.com/freeuselicense).

All other material is created by the us and covered by the open-source APACHE 2.0 License.